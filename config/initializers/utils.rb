String.class_eval do
  def camelize
    self.split("_").collect do |m|
      m.capitalize
    end.join("")
  end

  def constantize
    Object.const_get self
  end
end

Hash.class_eval do
  def deep_symbolize_keys
    deep_transform_keys{ |key| key.to_sym rescue key }
  end

  protected

  def deep_transform_keys(&block)
    result = {}
    each do |key, value|
      result[yield(key)] = value.is_a?(Hash) ? value.deep_transform_keys(&block) : value
    end
    result
  end
end

def configure_widgets!(app)

    app.class_eval do

      auth_providers = {}

      #@scheduler = Rufus::Scheduler.new

      Dir.glob("#{app.settings.root}/controllers/*.rb").each do |file|

        class_name = file.split("/").last.gsub(".rb","").camelize
        # skips application_controller
        next if class_name.eql? 'ApplicationController'

        require file
        klass = class_name.constantize

        p "Configuring controller: #{klass}"

        if !klass.resource_path.nil? && !klass.resource_routes.nil?

          p "Configuring controller: #{klass} at route #{klass.resource_path}"

          klass.resource_routes.each do |k, routes|
            routes.each do |path, method_name|

              route = (path == '/') ? '/?' : path
              p "Setting up route #{klass.resource_path}#{route} via #{k} to #{klass}"

              app.send(k, "#{klass.resource_path}#{route}") do
                p "Invoking #{method_name} on controller #{klass}"
                klass.new(request, params).send(method_name)
              end
            end
          end

          # unless klass.update_every.nil?
          #   p "> Setting up update schedule for #{klass} every #{klass.update_every}"
          #   @scheduler.every klass.update_every do
          #     p "*** Updating data for #{klass}"
          #     klass.new(nil, nil).update
          #   end
          # end

        else
          raise StandardError
        end

      end #end of Dir loop

    end #end of class_eval

  end
