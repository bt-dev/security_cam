require_relative "spec_helper"
require_relative "../secure_cam.rb"

def app
  SecureCam
end

describe SecureCam do
  it "responds with a welcome message" do
    get '/'

    last_response.body.must_include 'Welcome to the Sinatra Template!'
  end
end
