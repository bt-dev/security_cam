# Load path and gems/bundler
$LOAD_PATH << File.expand_path(File.dirname(__FILE__))

RACK_ENV = ENV['RACK_ENV'] || 'development' unless defined? RACK_ENV

require "bundler"
Bundler.require

# Local config
require "find"

%w{config/initializers lib app/controllers}.each do |load_path|
  Find.find(load_path) { |f|
    require f unless f.match(/\/\..+$/) || File.directory?(f)
  }
end

# Load app
require "./app/app"
run SecureCam
