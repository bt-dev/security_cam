require 'json'
require 'sinatra'
require 'sinatra/reloader'
require 'better_errors'
require 'binding_of_caller'
#require 'unirest'

class SecureCam < Sinatra::Application
  register Sinatra::Reloader

  set :session_secret, '*&(^B234'
  use Rack::Session::Cookie

  configure :development do
    use BetterErrors::Middleware
    BetterErrors.application_root = __dir__
  end

  set :root, File.dirname(__FILE__)
  set :public_folder => "#{settings.root}/../public", :static => true
  # set :views, "#{settings.root}/app/views"
  also_reload '/app/views/'

  use Rack::Auth::Basic, "Restricted Area" do |username, password|
    [username, password] == ['admin', 'pochacco']
  end

  configure do
    set :bind, '0.0.0.0'
  end

  configure_widgets! SecureCam

  get '/' do
    slim :'home/index', :layout => :layout
  end
end
