class CamController < ApplicationController

  class << self

    def resource_path
      "/cam"
    end

    def resource_routes
      {
        get: {
          "/" => :index,
          "/:id" => :show
        },
        post: {
          "/" => :create
        }
      }
    end

  end

  def index

    @view_title = "Benvenuto"
    respond_to do
      format :html do
        slim :"cam/index"
      end
    end
  end

  def show
    respond_to do
      format(:json) do
        {something: 'strange is happening'}.to_json
      end
    end
  end
end
