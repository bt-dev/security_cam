require 'json'
require 'slim'

class String
  def underscore
    self.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end
end

class ApplicationController

  def initialize(request,params)
    @request = request
    @params = params
    @formats = {}
  end

  class << self

    def resource_path
      nil
    end

    def resource_routes
      {
        get: {
          "/" => :index,
          "/:id" => :show
        },
        post: {
          "/" => :create
        }
      }
    end

  end

  ## default CRUD methods
  def index
    respond_to do |format|
      format(:json) do
        {custom: 'my custom json'}.to_json
      end
    end
  end

  def show
  end

  def create
  end

  def update
  end

  def destroy
  end

  ## - default CRUD methods

  def handle
    p "Instance: " + self.inspect
    send(action)
    render_action
  end

  def request_format
    request_format_overrides
    @request_format ||= case @request.env['HTTP_ACCEPT'].split(",").first
    when 'text/html'
      :html
    when 'application/json'
      :json
    else
      :html
    end
  end

  def request_format_overrides
    unless (@request.path.split("/").last =~ /(\.[a-zA-Z]+)/i).nil?
      @request_format ||= @request.path.split("/").last.match(/\.([a-zA-Z]+)/i)[1].to_sym
    end
  end

  def response_type
    case request_format
    when :json
      'application/json'
    else
      'text/html'
    end
  end

  def respond_to
    if block_given?
      yield
    end
  end

  def format(format_name, &block)
    if block_given?
      @formats[format_name] = block
    end
  end

  protected

  def render_action
    unless @formats[request_format].nil?
      [@formats[request_format].call, response_type, 200]
    else
      [default_render, response_type, 200]
    end
  end

  private

  # def render(view_name)
  #   dir = File.expand_path(".") + "/app/views/" + views_path
  #   static_file_path = dir + "/" + view_name.to_s + "." + request_format.to_s
  #
  #   template_file_path = dir + "/" + view_name.to_s + "." + request_format.to_s + ".slim"
  #
  #   if File.directory?(dir)
  #     if File.exists?(static_file_path)
  #       File.read(static_file_path)
  #     elsif File.exists?(template_file_path)
  #       Slim::Template.new{File.read(template_file_path)}.render(self)
  #     end
  #   else
  #     "Error: missing template"
  #   end
  #
  #
  # end

  def views_path
    self.class.to_s.underscore.gsub("_controller","")
  end

  def default_render
    case request_format
    when :json
      {}.to_json
    else
      "Nothing to see in here"
    end
  end

  def action
    @action ||= self.class.resource_method(request)
  end

  def request
    @request
  end

  def params
    @params
  end

  def self.resource_method(req)

    case req.request_method
    when 'GET'
      (req.path =~ /#{self.name.underscore.gsub('_controller','')}\/[0-9]+/i).nil? ? :index : :show
    when 'POST'
      :create
    when 'PUT', 'PATCH'
      :update
    when 'DELETE'
      :destroy
    else #not allowed
      ['Not allowed', 'text/html', 405]
    end
    #JSON.parse( req.body.read )
  end

end
